DROP TABLE cidades IF EXISTS;
CREATE TABLE cidades  (
    id VARCHAR(10),
    uf VARCHAR(64),
    nome VARCHAR(250),
    capital VARCHAR(5),
    lon VARCHAR(400),
    lat VARCHAR(400),
    no_accent VARCHAR(400),
    alternative_names VARCHAR(400),
    microregion VARCHAR(400),
    mesoregion VARCHAR(400),
);
