package com.senior;

import org.apache.commons.lang3.builder.ToStringBuilder;
/**
 * Contains the information of a single anime
 *
 * @author Michael C Good michaelcgood.com
 */

public class Cidades {
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getNo_accent() {
        return no_accent;
    }

    public void setNo_accent(String no_accent) {
        this.no_accent = no_accent;
    }

    public String getAlternative_names() {
        return alternative_names;
    }

    public void setAlternative_names(String alternative_names) {
        this.alternative_names = alternative_names;
    }

    public String getMicroregion() {
        return microregion;
    }

    public void setMicroregion(String microregion) {
        this.microregion = microregion;
    }

    public String getMesoregion() {
        return mesoregion;
    }

    public void setMesoregion(String mesoregion) {
        this.mesoregion = mesoregion;
    }
    
	private String id;

	private String uf;
	private String nome;
        private String capital;
        private String lon;
        private String lat;
        private String no_accent;
        private String alternative_names;
        private String microregion;
        private String mesoregion;   
	
	public Cidades(){
		
	}
        
        public Cidades(String id, String uf, String nome, String capital, String lon, String lat, String no_accent, String alternative_names, String microregion, String mesoregion) {
            this.id = id;
            this.uf = uf;
            this.nome = nome;
            this.capital = capital;
            this.lon = lon;
            this.lat = lat;
            this.no_accent = no_accent;
            this.alternative_names = alternative_names;
            this.microregion = microregion;
            this.mesoregion = mesoregion;
        }
	
	
	

	
	   @Override
	    public String toString() {
		   return new ToStringBuilder(this)
                                    .append("id", this.id)
                                    .append("uf", this.uf)
                                    .append("nome", this.nome)
                                    .append("capital", this.capital)
                                    .append("lon", this.lon)
                                    .append("lat", this.lat)
                                    .append("no_accent", this.no_accent)
                                    .append("alternative_names", this.alternative_names)
                                    .append("microregion", this.microregion)
                                    .append("mesoregion", this.mesoregion)
                                    .toString();
	   }


}
