package com.senior;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.batch.item.ItemProcessor;

public class CidadeProcessor implements ItemProcessor<Cidades, Cidades> {
	
    private static final Logger log = LoggerFactory.getLogger(CidadeProcessor.class);
    
    @Override
    public Cidades process(final Cidades Cidades) throws Exception {
    	
    	final String id = Cidades.getId();
        final String uf = Cidades.getUf();
        final String nome = Cidades.getNome();
        final String capital = Cidades.getCapital();
        final String lon = Cidades.getLon();
        final String lat = Cidades.getLat();
        final String no_accent = Cidades.getNo_accent();
        final String alternative_names = Cidades.getAlternative_names();
        final String microregion = Cidades.getMicroregion();
        final String mesoregion = Cidades.getMesoregion();
        
        final Cidades transformCidade = new Cidades(id,uf,nome,capital,lon,lat,no_accent,alternative_names,microregion,mesoregion);

        log.info("Converting (" + Cidades + ") into (" + transformCidade + ")");

        return transformCidade;
    }

}
