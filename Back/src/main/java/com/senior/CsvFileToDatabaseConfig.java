package com.senior;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@EnableBatchProcessing
@Configuration
public class CsvFileToDatabaseConfig {
	
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    public DataSource dataSource;
    
    // begin reader, writer, and processor

    
    @Bean
    public FlatFileItemReader<Cidades> csvCidadesReader(){
        FlatFileItemReader<Cidades> reader = new FlatFileItemReader<Cidades>();
        reader.setResource(new ClassPathResource("cidades.csv"));
        reader.setLineMapper(new DefaultLineMapper<Cidades>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames(new String[] { "id", "uf", "nome", "capital", "lon", "lat", "no_accent", "alternative_names", "microregion", "mesoregion"});
            }});
            setFieldSetMapper(new BeanWrapperFieldSetMapper<Cidades>() {{
                setTargetType(Cidades.class);
            }});
        }});
        return reader;
    }


	@Bean
	ItemProcessor<Cidades, Cidades> csvCidadeProcessor() {
		return new CidadeProcessor();
	}

	@Bean
	public JdbcBatchItemWriter<Cidades> csvCidadeWriter() {
		 JdbcBatchItemWriter<Cidades> csvAnimeWriter = new JdbcBatchItemWriter<Cidades>();
		 csvAnimeWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Cidades>());
		 csvAnimeWriter.setSql("INSERT INTO cidades (id, uf, nome, capital, lon, lat, no_accent, alternative_names, microregion, mesoregion) VALUES (:id, :uf, :nome, :capital, :lon, :lat, :no_accent, :alternative_names, :microregion, :mesoregion)");
		 csvAnimeWriter.setDataSource(dataSource);
	        return csvAnimeWriter;
	}

	 // end reader, writer, and processor

    // begin job info
	@Bean
	public Step csvFileToDatabaseStep() {
		return stepBuilderFactory.get("csvFileToDatabaseStep")
				.<Cidades, Cidades>chunk(1)
				.reader(csvCidadesReader())
				.processor(csvCidadeProcessor())
				.writer(csvCidadeWriter())
				.build();
	}

	@Bean
	Job csvFileToDatabaseJob(JobCompletionNotificationListener listener) {
		return jobBuilderFactory.get("csvFileToDatabaseJob")
				.incrementer(new RunIdIncrementer())
				.listener(listener)
				.flow(csvFileToDatabaseStep())
				.end()
				.build();
	}
	 // end job info
}
