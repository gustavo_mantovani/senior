import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DashboardService } from './../dashboard.service'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  form: FormGroup;
  formErrors: any;

  public itensByState : number = 0;
  public listSates : Array<any> = [
      {
          code: 'SP',
          name: 'São Paulo'
      },
      {
          code: 'BA',
          name: 'Bahia'
      }
  ]
  private baseData : Array<any> = []
  public listData : Array<any> = [
      {
          cidade: 'São Paulo',
          uf: 'SP'
      },
      {
          cidade: 'Bady Bassitt',
          uf: 'SP'
      },
      {
          cidade: 'Salvador',
          uf: 'BA'
      },
  ]

  constructor(private dashboardService: DashboardService,
              private formBuilder: FormBuilder) {
      // dashboardService.getCities().subscribe((res) => {
      //     if(res)
      //       this.listData = res
      // })
  }

  ngOnInit() {
      this.formErrors = {
          city: {},
          state: {}
      };
      this.form = this.formBuilder.group({
          city: '',
          state: '',
      });
      this.itensByState = this.listData.length
      this.baseData = this.listData
  }

  public calcState(e : any) {
      this.itensByState = (e.target.value == 'all') ? this.baseData.length :
        this.baseData.filter((item) => item.uf == e.target.value).length
  }

  public filterEvent(form) {
      this.listData = this.baseData.filter((item) => {
          let isValid = false
          if(form.controls.state.value != "" && form.controls.city.value != "") {
              if(form.controls.state.value == item.uf && item.cidade.indexOf(form.controls.city.value) != -1)
                isValid = true
          } else if(form.controls.state.value != "" && form.controls.state.value == item.uf)
            isValid = true
          else if(form.controls.city.value != "" && item.cidade.indexOf(form.controls.city.value) != -1)
            isValid = true
          return isValid
      })
  }
}
