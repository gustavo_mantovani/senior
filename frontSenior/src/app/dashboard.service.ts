import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import 'rxjs/Rx';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DashboardService {
  constructor(private http: HttpClient) { }

  /**
  * Do request to get list of cities
  * @return Observable<any> These contains the cities from RESTful
  **/
  getCities() : Observable<any> {
      return this.http.get('http://localhost:8080/cidades/')
  }
}
